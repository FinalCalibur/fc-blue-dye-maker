package scripts.fc.fcbluedyemaker;

import java.awt.event.KeyEvent;

import org.tribot.api.General;
import org.tribot.api.input.Keyboard;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Login;
import org.tribot.api2007.Login.STATE;

public class SpacebarThread extends Thread
{
	private final long SLEEP_TIME = 5000;
	private final int SPACE_BAR_KEYCODE = KeyEvent.VK_SPACE;
	private final Condition HOLD_KEY_CONDITION = holdKeyCondition();
	
	private FCBlueDyeMaker script;
	private boolean humanHasIntervened;
	
	public SpacebarThread(FCBlueDyeMaker script)
	{
		this.script = script;
		
		this.setName("FC Spacebar Thread");
		
		this.start();
	}
	
	public void run()
	{
		while(script != null)
		{
			try
			{
				if(Login.getLoginState() == STATE.INGAME)
				{
					General.println("SpacebarThread loop");
					
					humanHasIntervened = false;
				
					Keyboard.holdKey(' ', SPACE_BAR_KEYCODE, HOLD_KEY_CONDITION);
					
					Thread.sleep(SLEEP_TIME);
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	private Condition holdKeyCondition()
	{
		return new Condition()
		{
			@Override
			public boolean active()
			{
				return humanHasIntervened || Login.getLoginState() != STATE.INGAME;
			}
			
		};
	}
	
	public void setHumanHasIntervened(boolean humanHasIntervened)
	{
		this.humanHasIntervened = humanHasIntervened;
	}
}
