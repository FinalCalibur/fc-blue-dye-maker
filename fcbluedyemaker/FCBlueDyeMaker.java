package scripts.fc.fcbluedyemaker;

import java.awt.Graphics;

import org.tribot.api.Clicking;
import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.input.Mouse;
import org.tribot.api.interfaces.Positionable;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Camera;
import org.tribot.api2007.Camera.ROTATION_METHOD;
import org.tribot.api2007.Game;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Login;
import org.tribot.api2007.Login.STATE;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSNPC;
import org.tribot.api2007.types.RSTile;
import org.tribot.api2007.util.DPathNavigator;
import org.tribot.script.Script;
import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.Ending;
import org.tribot.script.interfaces.KeyActions;
import org.tribot.script.interfaces.Painting;
import org.tribot.script.interfaces.Starting;

import scripts.fc.fcpaint.FCPaint;
import scripts.fc.fcpaint.FCPaintable;
import scripts.fc.utils.FCInventoryListener;
import scripts.fc.utils.FCInventoryObserver;

@ScriptManifest(
		authors     = { 
		    "Final Calibur",
		}, 
		category    = "Tools", 
		name        = "FC Blue Dye Maker", 
		version     = 0.1, 
		description = "Makes blue dyes at Draynor Village", 
		gameMode    = 1)

public class FCBlueDyeMaker extends Script implements FCPaintable, Painting, Starting, Ending, KeyActions, FCInventoryListener
{
	//Local constants
	private final ScriptManifest 		MANIFEST = (ScriptManifest)this.getClass().getAnnotation(ScriptManifest.class); //The script manifest object
	private final long 					SLEEP_TIME = 100;	//Script sleep time
	private final String				PRODUCT_NAME = "Blue dye";	//Name of the item we're making
	private final String 				NPC_NAME = "Aggie";	//The name of the npc we're going to talk to
	private final Positionable 			NPC_TILE = new RSTile(3085, 3259, 0);	//Tile near npc we are interacting with
	private final int 					MINIMUM_NPC_DISTANCE = 3;	//The minimum distance from the npc we can be
	private final int 					MINIMUM_CAMERA_ANGLE = 80; //The lowest camera angle the script will allow
	private final Object[][] 			REQUIRED_MATERIALS = {{"Woad leaf", 2}, {"Coins", 5}}; //Required materials for the script
	private final DPathNavigator 		DPATH = new DPathNavigator();	//For more accurate walking (shorter distances than WebWalking)
	private final RSArea 				BANK_AREA = new RSArea(new RSTile(3092, 3245, 0), new RSTile(3095, 3241, 0)); //Don't want to use WebWalking, so define our own bank area
	private final SpacebarThread 		SPACEBAR_THREAD = new SpacebarThread(this);	//Separate thread for holding spacebar (at all times)
	private final FCInventoryObserver 	INV_OBSERVER = new FCInventoryObserver();	//This is how we'll observe our vials being added to inv
	
	//Local variables
	private FCPaint 	paint = new FCPaint(this);	//Handles our painting
	private boolean 	hasMaterials = true;	//Whether or not we have materials to make dyes (woad leaves)
	private int 		dyesMade;	//# of dyes we've made
	private int 		npcId = -1;	//Cache for NPC ID (after we find it from searching by name)
	private String 		status = "STARTING UP"; //Status message for paint (what the script is currently doing)
	
	@Override
	public void run()
	{
		while(hasMaterials) //While we have materials (if we don't, we can't make dyes and script ends)
		{
			if(Login.getLoginState() != STATE.INGAME) //IF(we are not in game)
			{
				continue; //Skip this loop iteration
			}
			
			adjustCamera(); //Keep the angle up high (easier to click on npc)
			
			//IF(we don't have materials OR inventory is full)
			if(!hasMatsInInv() || Inventory.isFull())
			{
				handleBanking(); //Go check bank for materials
			}
			else //ELSE(has materials and can make dye)
			{
				if(!isAtNpc()) //IF(player isn't near npc)
				{
					goToNpc(); //Walk to the npc
				}
				else //ELSE(player is near npc)
				{
					makeDye(); //Make the dyes
				}
			}
			
			sleep(SLEEP_TIME); //Sleep to reduce CPU usage
		}
	}
	
	private boolean isAtNpc()
	{
		return Player.getPosition().distanceTo(NPC_TILE) <= MINIMUM_NPC_DISTANCE;
	}
	
	private void adjustCamera()
	{
		if(Camera.getCameraAngle() < MINIMUM_CAMERA_ANGLE)
		{
			Camera.setCameraAngle(General.random(MINIMUM_CAMERA_ANGLE, 100));
		}
	}
	
	private void goToNpc()
	{
		status = "GO TO NPC";
		
		DPATH.traverse(NPC_TILE);
	}
	
	private void makeDye()
	{
		status = "MAKE DYE";
		
		RSNPC[] npcs;
		
		if(npcId != -1) //IF(we have the npc id cached)
		{
			npcs = NPCs.find(npcId); //Search for npc by cached id
		}
		else //ELSE(we don't have npc id cached)
		{
			npcs = NPCs.find(NPC_NAME); //Search for npc by name
		}
		
		if(npcs.length > 0)
		{
			RSNPC npc = npcs[0];
			
			if(npc != null)
			{
				npcId = npc.getID();
				
				if(!npc.isClickable())
				{
					Camera.turnToTile(npc);
				}
				
				if(Game.isUptext((String)REQUIRED_MATERIALS[0][0])) //IF(we have the woad leaf selected)
				{
					if(DynamicClicking.clickRSNPC(npc, 1)) //IF(we successfully clicked on npc)
					{
						/*
						 * Static sleeps are normally bad, use Timing.waitCondition whenever possible
						 * 
						 * I was lazy and the spacebar gets through dialogue so quickly that it doesn't really matter
						 */
						sleep(800, 1200); //Short sleep 
					}
				}
				else
				{
					Clicking.click(Inventory.find((String)REQUIRED_MATERIALS[0][0])); //Click woad leaf
				}
			}
		}	
	}
	
	private void handleBanking()
	{
		if(!Banking.isInBank()) //IF(we aren't in the bank)
		{
			status = "GO TO BANK";
			
			if(Game.isUptext((String)REQUIRED_MATERIALS[0][0])) //This is a failsafe for if we have a woad leaf selected - Would mess up DPath
			{
				Mouse.click(1); //Click off the woad leaf selection
			}
			
			DPATH.traverse(BANK_AREA.getRandomTile()); //Walk to the bank
		}
		else //ELSE (we are in the bank)
		{
			if(Banking.openBank()) //IF(we attempted to open the bank and succeeded)
			{
				sleep(600, 1200); //Little sleep so the bank can load
				
				if(Inventory.isFull()) //Inventory full - need to deposit items
				{
					depositDye();
				}
				
				boolean hasMatsInInv = hasMatsInInv();
				boolean hasMatsInBank = hasMatsInBank();
				
				if(!hasMatsInInv && hasMatsInBank) //IF(doesn't have mats in inventory but does in bank)
				{
					withdrawMaterials(); //Withdraw materials from bank
				}
				else if(!hasMatsInInv && !hasMatsInBank) //ELSE IF(doesn't have mats in both inventory and bank)
				{
					hasMaterials = false; //End script
				}
			}
		}
	}
	
	private void depositDye()
	{
		Banking.deposit(0, PRODUCT_NAME); //Deposit all of the product item
	}
	
	private boolean hasMatsInBank()
	{
		for(int i = 0; i < REQUIRED_MATERIALS.length; i++) //Loop through each required material
		{
			String name = (String)REQUIRED_MATERIALS[i][0];
			int amt = (int)REQUIRED_MATERIALS[i][1];
			
			RSItem[] bankedItems = Banking.find(name);
			
			if(bankedItems.length > 0)
			{
				RSItem item = bankedItems[0];
				
				if(item.getStack() < amt)
				{
					return false;
				}
			}
		}
		
		return true;
	}
	
	private boolean hasMatsInInv()
	{
		for(int i = 0; i < REQUIRED_MATERIALS.length; i++) //Loop through each required material
		{
			String name = (String)REQUIRED_MATERIALS[i][0];
			int amt = (int)REQUIRED_MATERIALS[i][1];
			
			if(Inventory.getCount(name) < amt) //IF(we don't have material in inventory)
			{
				return false; //Material is not in inventory, return false
			}
		}
	
		return true; //return true, we do have materials to continue
	}
	
	private void withdrawMaterials()
	{	
		status = "WITHDRAW MATERIALS";
		
		for(int i = 0; i < REQUIRED_MATERIALS.length; i++) //Loop through each required material
		{
			//Withdraws the entire stack of materials (doesn't have to be noted for this script)
			Banking.withdraw(0, (String)REQUIRED_MATERIALS[i][0]);
		}
	}

	@Override
	public String[] getPaintInfo()
	{
		return new String[]{"Time ran: " + paint.getTimeRan(), "Dyes made: " + dyesMade,
				"Dyes per hour: " + paint.getPerHour(dyesMade), "Status: " + status};
	}

	@Override
	public void onPaint(Graphics g)
	{
		paint.paint(g);
	}

	@Override
	public void onEnd()
	{
		println("Thank you for running " + MANIFEST.name() + " v" + MANIFEST.version() + " by " + MANIFEST.authors()[0]);
		
		SPACEBAR_THREAD.setHumanHasIntervened(true);
	}

	@Override
	public void onStart()
	{
		println("Started " + MANIFEST.name() + " v" + MANIFEST.version() + " by " + MANIFEST.authors()[0]);
		
		Camera.setRotationMethod(ROTATION_METHOD.ONLY_MOUSE);
		
		INV_OBSERVER.addListener(this);
	}

	@Override
	public void keyPressed(int arg0, boolean arg1)
	{}

	@Override
	public void keyReleased(int keyCode, boolean isBot)
	{
		if(!isBot)
		{
			SPACEBAR_THREAD.setHumanHasIntervened(true);
		}
	}

	@Override
	public void keyTyped(int arg0, boolean arg1)
	{}

	@Override
	public void inventoryAdded(int id, int count)
	{
		if(count == 1) //BAD way to do it generally, but for this script it should work
		{
			dyesMade++;
		}
	}

	@Override
	public void inventoryRemoved(int id, int count)
	{}

}
