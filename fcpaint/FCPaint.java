package scripts.fc.fcpaint;

import java.awt.Color;
import java.awt.Graphics;

import org.tribot.api.Timing;

public class FCPaint
{
	private final Color PAINT_COLOR = Color.WHITE; //The color of the paint text
	private final int	PAINT_X		= 4; //The x coordinate for the paint text
	private final int	PAINT_BOT_Y	= 336; //The y coordinate for the paint string on the bottom
	private final int	PAINT_SPACE	= 15; //The space between paint fields		
	private final long  START_TIME 	= Timing.currentTimeMillis(); //Start time of script
	
	private FCPaintable paintable; //Paintable object we're painting (we get our paint info from this)
	
	public FCPaint(FCPaintable paintable)
	{
		this.paintable = paintable;
	}
	
	public void paint(Graphics g)
	{
		//set paint text color
		g.setColor(PAINT_COLOR);
		
		String[] info = paintable.getPaintInfo();
		
		//FOR(each paint information field in paintInfo)
		for(int index = 0; index < info.length; index++)
		{
			//draw paint field at the appropriate position on the screen, as defined by constants
			g.drawString(info[index], PAINT_X, PAINT_BOT_Y - (PAINT_SPACE * (info.length - (index + 1))));
			
		} //END FOR
	}
	
	/**
	 *	String getTimeRan() - Returns a proper string that represents the total running time of the script.
	 *
	 *	START getTimeRan()
	 *		return the properly formatted string
	 *	END getTimeRan()
	 */
	public String getTimeRan()
	{	
		//return the properly formatted string
		return Timing.msToString(Timing.timeFromMark(START_TIME));
		
	} //END getTimeRan()
	
	/**
	 *	String getPerHour(int amount) - Returns the projected amount per hour based on your current amount
	 *
	 *	START getTimeRan(int amount)
	 *		return the projected amount per hour
	 *	END getTimeRan(int amount)
	 */
	public long getPerHour(int amount)
	{	
		//return the projected amount per hour
		return Math.round(amount / (Timing.timeFromMark(START_TIME) / 3600000.0));
	}
}
